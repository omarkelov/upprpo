package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.requests.AuthRequest;
import ru.nsu.fit.upprpo.controllers.requests.RegistrationRequest;
import ru.nsu.fit.upprpo.database.entities.User;
import ru.nsu.fit.upprpo.security.services.UserService;

import java.util.stream.Stream;

class AuthControllerTest extends ControllerTestBase {
    @Autowired
    private UserService userService;

    @BeforeEach
    void setUp() {
        User registeredUser = new User();
        registeredUser.setPassword("CorrectPassword");
        registeredUser.setUsername("RegisteredUser");
        userService.saveUser(registeredUser);
    }

    static Stream<Arguments> loginTestArgs() {
        return Stream.of(
                Arguments.of("UnknownUser", "123456", 401),
                Arguments.of("RegisteredUser", "IncorrectPassword", 401),
                Arguments.of("RegisteredUser", "CorrectPassword", 200)
        );
    }

    @ParameterizedTest
    @MethodSource("loginTestArgs")
    void loginTest(String username, String password, int expectedStatus) throws Exception {
        AuthRequest request = new AuthRequest();
        request.setLogin(username);
        request.setPassword(password);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(Endpoints.LOGIN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(request));

        var result = mock.perform(requestBuilder).andReturn();

        Assertions.assertEquals(expectedStatus, result.getResponse().getStatus());
    }

    @Test
    void registrationTest() throws Exception {

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setLogin("Ivan");
        registrationRequest.setPassword("123456");

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(Endpoints.REGISTER)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(registrationRequest));

        var firstAttemptResult = mock.perform(requestBuilder).andReturn();

        Assertions.assertEquals(200, firstAttemptResult.getResponse().getStatus());

        var secondAttemptResult = mock.perform(requestBuilder).andReturn();

        Assertions.assertEquals(400, secondAttemptResult.getResponse().getStatus());
    }


}
