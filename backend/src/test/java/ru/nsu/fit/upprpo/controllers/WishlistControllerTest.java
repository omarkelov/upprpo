package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import ru.nsu.fit.upprpo.config.UserDetailsImpl;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponse;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.RoleRepository;
import ru.nsu.fit.upprpo.security.services.UserService;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WishlistControllerTest extends ControllerTestBase {

    @Autowired
    private WishlistController wishlistController;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService service;

    public HashMap<String, User> users;


    public static User currentUser;

    @BeforeEach
    public void setup() {

        Category category = new Category();
        category.setName("Category");
        category.setChildren(new HashSet<>());
        categoryRepository.save(category);

        Product product = new Product();
        product.setName("Product");
        product.setManufacturer("Company");
        product.setId(1L);
        product.setDescription("Description");
        product.setPrice(new BigDecimal(12));
        product.setCategory(category);
        productRepository.save(product);


        users = new HashMap<>();

        Role adminRole = new Role();
        adminRole.setRoleType(RoleType.ROLE_ADMIN);

        Role customerRole = new Role();
        customerRole.setRoleType(RoleType.ROLE_CUSTOMER);

        roleRepository.save(adminRole);
        roleRepository.save(customerRole);

        User user = new User();
        user.setRole(customerRole);
        user.setUsername("user");
        user.setPassword(passwordEncoder.encode("user"));
        user.setWishlist(new HashSet<>());
        users.put(user.getUsername(), user);
        service.saveUser(user);

        mock = MockMvcBuilders.standaloneSetup(wishlistController)
                .setCustomArgumentResolvers(new TestingDetailsResolver()).build();
    }

    private static class TestingDetailsResolver implements HandlerMethodArgumentResolver {

        @Override
        public boolean supportsParameter(MethodParameter parameter) {
            return parameter.getParameterType().isAssignableFrom(UserDetailsImpl.class);
        }

        @Override
        public Object resolveArgument(
                MethodParameter parameter,
                ModelAndViewContainer mavContainer,
                NativeWebRequest webRequest,
                WebDataBinderFactory binderFactory) throws Exception {
            return UserDetailsImpl.fromUserEntityToCustomUserDetails(currentUser);
        }

    }

    @AfterEach
    void cleanUp() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        roleRepository.deleteAll();
        service.deleteAll();
    }

    @Test
    void wishlistTest() throws Exception {
        long productId = productRepository.findAll().get(0).getId();
        currentUser = users.get("user");
        {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .get(Endpoints.WISHLIST)
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();

            assertTrue(getWishlist(res.getResponse().getContentAsString()).isEmpty());
        }
        for (int i = 0; i < 5; ++i) {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .put(Endpoints.WISHLIST)
                            .param("productId", String.valueOf(productId))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();
            var wishlist = getWishlist(res.getResponse().getContentAsString());
            assertEquals(1, wishlist.size());
            assertEquals(productId, wishlist.get(0).getId());
        }
        for (int i = 0; i < 5; ++i) {
            Product product = productRepository.findById(productId);
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .delete(Endpoints.WISHLIST)
                            .param("productId", String.valueOf(productId))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();
            var wishlist = getWishlist(res.getResponse().getContentAsString());
            assertTrue(getWishlist(res.getResponse().getContentAsString()).isEmpty());
        }
    }

    @Test
    void badProductWishlistTest() throws Exception {
        // non-existent product
        long productId = productRepository.findAll()
                .stream()
                .map(Product::getId)
                .max(Long::compare)
                .map(a -> a + 1)
                .orElse(1L);

        currentUser = users.get("user");
        {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .put(Endpoints.WISHLIST)
                            .param("productId", String.valueOf(productId))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();
            assertEquals(HttpStatus.BAD_REQUEST.value(), res.getResponse().getStatus());
        }
        {
            MockHttpServletRequestBuilder requestBuilder =
                    MockMvcRequestBuilders
                            .delete(Endpoints.WISHLIST)
                            .param("productId", String.valueOf(productId))
                            .contentType(MediaType.APPLICATION_JSON);

            var res = mock.perform(requestBuilder).andReturn();
            assertEquals(HttpStatus.BAD_REQUEST.value(), res.getResponse().getStatus());
        }
    }

    private static List<ProductResponse> getWishlist(String text) throws Exception {
        Type listType = new TypeToken<ArrayList<ProductResponse>>() {
        }.getType();
        return new Gson().fromJson(text, listType);
    }
}
