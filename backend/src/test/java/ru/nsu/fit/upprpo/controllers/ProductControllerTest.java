package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponse;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Stream;

class ProductControllerTest extends ControllerTestBase {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @BeforeEach
    public void setUp() {

        Category category1 = new Category();
        category1.setChildren(Collections.emptySet());
        category1.setName("Memy ahaha");
        categoryRepository.save(category1);

        for (int i = 1; i <= 10; i++) {
            Product product = new Product();
            product.setName("Porridge_" + i);
            product.setManufacturer("Erokha_" + i);
            product.setId((long) i);
            product.setDescription("Om-nom, porridge");
            product.setPrice(new BigDecimal(12 + i));
            if (i <= 5) product.setCategory(category1);
            productRepository.save(product);
        }
    }


    static Stream<RequestValues> testSearchParams() {
        return Stream.of(
                new RequestValues(10),
                new RequestValues(10).addParam("name", "%Porridge_%"),
                new RequestValues(8).addParam("priceMin", "15"),
                new RequestValues(0).addParam("priceMin", "30"),
                new RequestValues(-1).addParam("priceMin", "kek"),
                new RequestValues(3).addParam("priceMax", "15"),
                new RequestValues(10).addParam("priceMax", "30"),
                new RequestValues(-1).addParam("priceMax", "kek"),
                new RequestValues(10).addParam("manufacturer", "%ro%"),
                new RequestValues(0).addParam("manufacturer", "%ro"),
                new RequestValues(0).addParam("manufacturer", "ro"));
    }

    @ParameterizedTest
    @MethodSource("testSearchParams")
    void testSearch(RequestValues values) throws Exception {

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(Endpoints.PRODUCTS_SEARCH).contentType(MediaType.APPLICATION_JSON);

        for (var entry : values.params.entrySet()) {
            requestBuilder = requestBuilder.param(entry.getKey(), entry.getValue());
        }

        String response = mock.perform(requestBuilder).andReturn().getResponse().getContentAsString();

        Gson gson = new Gson();

        Product[] result = gson.fromJson(response, Product[].class);

        if (values.number == -1) {
            Assertions.assertNull(result);
        } else {
            Assertions.assertEquals(values.number, result.length);
        }
    }

    static Stream<RequestValues> testGetProductParams() {
        return Stream.of(
                new RequestValues(-1).addParam("productId", ""),
                new RequestValues(1).addParam("productId", "1"),
                new RequestValues(0).addParam("productId", "-1"),
                new RequestValues(0).addParam("productId", "kek"));
    }

    @ParameterizedTest
    @MethodSource("testGetProductParams")
    void testGetProduct(RequestValues values) throws Exception {

        String param;

        try {
            long id = Long.parseLong(values.params.get("productId"));
            if (id > 0) {
                param = productRepository.findAll().get(0).getId().toString();
            } else {
                param = values.params.get("productId");
            }
        } catch (NumberFormatException e) {
            param = values.params.get("productId");
        }

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.PRODUCT + "/" + param)
                        .contentType(MediaType.APPLICATION_JSON);


        var res = mock.perform(requestBuilder).andReturn();

        if (values.number == -1) {
            Assertions.assertEquals(404, res.getResponse().getStatus());
            return;
        }

        String response = res.getResponse().getContentAsString();

        Gson gson = new Gson();
        Product result = gson.fromJson(response, Product.class);

        if (values.number == 0) {
            Assertions.assertNull(result);
        } else {
            Assertions.assertNotNull(result);
        }
    }

    @Test
    void testGetProducts() throws Exception {

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.PRODUCTS)
                        .contentType(MediaType.APPLICATION_JSON);

        var res = mock.perform(requestBuilder).andReturn();

        String response = res.getResponse().getContentAsString();

        Gson gson = new Gson();
        Product[] result = gson.fromJson(response, Product[].class);

        Assertions.assertEquals(10, result.length);
    }

    static Stream<RequestValues> testGetByCategoryParams() {
        return Stream.of(
                new RequestValues(10).addParam("categoryId", ""),
                new RequestValues(5).addParam("categoryId", "1"),
                new RequestValues(5).addParam("categoryId", "-1"));
    }

    @ParameterizedTest
    @MethodSource("testGetByCategoryParams")
    void testGetByCategory(RequestValues values) throws Exception {

        String param;

        try {
            long id = Long.parseLong(values.params.get("categoryId"));
            if (id > 0) {
                param = categoryRepository.findAll().get(0).getId().toString();
            } else {
                param = values.params.get("categoryId");
            }
        } catch (NumberFormatException e) {
            param = values.params.get("categoryId");
        }

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.PRODUCTS + "/" + param)
                        .contentType(MediaType.APPLICATION_JSON);


        var res = mock.perform(requestBuilder).andReturn();

        if (!param.equals("") && !categoryRepository.existsById(Long.parseLong(param))) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), res.getResponse().getStatus());
        } else {
            String response = res.getResponse().getContentAsString();

            Gson gson = new Gson();
            ProductResponse[] result = gson.fromJson(response, ProductResponse[].class);
            if (param.equals("")) {
                Assertions.assertEquals(productRepository.findAll().size(), result.length);
            } else {
                Assertions.assertEquals(values.number, result.length);
            }
        }
    }

    @AfterEach
    public void cleanUp() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
    }

    private static class RequestValues {
        public HashMap<String, String> params = new HashMap<>();
        public int number;

        RequestValues(int number) {
            this.number = number;
        }

        public RequestValues addParam(String name, String value) {
            params.put(name, value);
            return this;
        }
    }
}
