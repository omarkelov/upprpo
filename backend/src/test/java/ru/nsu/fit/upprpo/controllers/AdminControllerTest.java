package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.junit.jupiter.api.AfterEach;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import ru.nsu.fit.upprpo.config.UserDetailsImpl;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.requests.CategoryRequest;
import ru.nsu.fit.upprpo.controllers.requests.ProductRequest;
import ru.nsu.fit.upprpo.database.entities.*;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.RoleRepository;
import ru.nsu.fit.upprpo.security.services.UserService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;

class AdminControllerTest extends ControllerTestBase {

    @Autowired
    private AdminController adminController;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService service;

    public HashMap<String, User> users;


    public static User currentUser;

    @BeforeEach
    public void setup() {

        Category category = new Category();
        category.setName("Category");
        category.setChildren(new HashSet<>());
        categoryRepository.save(category);

        Product product = new Product();
        product.setName("Product");
        product.setManufacturer("Company");
        product.setId(1L);
        product.setDescription("Description");
        product.setPrice(new BigDecimal(12));
        product.setCategory(category);
        productRepository.save(product);


        users = new HashMap<>();

        Role adminRole = new Role();
        adminRole.setRoleType(RoleType.ROLE_ADMIN);

        Role customerRole = new Role();
        customerRole.setRoleType(RoleType.ROLE_CUSTOMER);

        roleRepository.save(adminRole);
        roleRepository.save(customerRole);


        User admin = new User();
        admin.setRole(adminRole);
        admin.setUsername("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setWishlist(new HashSet<>());
        users.put(admin.getUsername(), admin);
        service.saveUser(admin);

        mock = MockMvcBuilders.standaloneSetup(adminController)
                .setCustomArgumentResolvers(new TestingDetailsResolver()).build();
    }


    private static class TestingDetailsResolver implements HandlerMethodArgumentResolver {

        @Override
        public boolean supportsParameter(MethodParameter parameter) {
            return parameter.getParameterType().isAssignableFrom(UserDetailsImpl.class);
        }

        @Override
        public Object resolveArgument(
                MethodParameter parameter,
                ModelAndViewContainer mavContainer,
                NativeWebRequest webRequest,
                WebDataBinderFactory binderFactory) throws Exception {
            return UserDetailsImpl.fromUserEntityToCustomUserDetails(currentUser);
        }

    }

    @AfterEach
    void cleanUp() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        roleRepository.deleteAll();
        service.deleteAll();
    }

    @Test
    void addProductTest() throws Exception {
        currentUser = users.get("admin");
        Category category = categoryRepository.findAll().get(0);

        ProductRequest productRequest = new ProductRequest();
        productRequest.setName("New Product");
        productRequest.setPrice(new BigDecimal("12.34"));
        productRequest.setDescription("Description");
        productRequest.setManufacturer("Manufacturer");
        productRequest.setCategoryId(category.getId());

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .post(Endpoints.CREATE_PRODUCT)
                        .content(new Gson().toJson(productRequest))
                        .contentType(MediaType.APPLICATION_JSON);

        mock.perform(requestBuilder);

        assertNotNull(productRepository.findByName(productRequest.getName()));
    }

    @Test
    void addCategoryTest() throws Exception {
        currentUser = users.get("admin");
        String parent = categoryRepository.findAll().get(0).getName();

        CategoryRequest categoryRequest = new CategoryRequest();
        categoryRequest.setName("New Category");
        categoryRequest.setParent(parent);
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .post(Endpoints.CREATE_CATEGORY)
                        .content(new Gson().toJson(categoryRequest))
                        .contentType(MediaType.APPLICATION_JSON);

        mock.perform(requestBuilder);

        Category category = categoryRepository.findByName(categoryRequest.getName()).orElseThrow();
        assertTrue(categoryRepository.findByName(parent).orElseThrow().getChildren().contains(category));
    }
}
