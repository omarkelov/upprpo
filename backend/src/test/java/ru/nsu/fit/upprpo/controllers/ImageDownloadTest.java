package ru.nsu.fit.upprpo.controllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Image;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ImageRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ImageDownloadTest extends ControllerTestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @BeforeEach
    void setUp() throws Exception {

        Category category = new Category();
        category.setName("Category");
        category.setChildren(new HashSet<>());
        categoryRepository.save(category);

        Product product = new Product();
        product.setName("Product");
        product.setManufacturer("Company");
        product.setDescription("Description");
        product.setPrice(new BigDecimal(12));
        product.setCategory(category);
        productRepository.save(product);

        Image image = new Image();
        image.setProduct(product);
        image.setContent(Objects.requireNonNull(getClass().getResourceAsStream("image.jpg")).readAllBytes());
        imageRepository.save(image);

    }

    @Test
    void downloadTest() throws Exception {
        Product product = productRepository.findAll().get(0);
        long imageId = imageRepository.findByProduct(product).get(0).getId();

        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders
                        .get(Endpoints.IMAGE + "/" + imageId)
                        .contentType(MediaType.IMAGE_JPEG_VALUE);

        var result = mock.perform(requestBuilder)
                .andExpect(status().is(200))
                .andReturn();

        var bytes = result.getResponse().getContentAsByteArray();

        Assertions.assertArrayEquals(imageRepository.findByProduct(product).get(0).getContent(), bytes);
    }

    @AfterEach
    void cleanup() {
        imageRepository.deleteAll();
        productRepository.deleteAll();
        categoryRepository.deleteAll();
    }
}
