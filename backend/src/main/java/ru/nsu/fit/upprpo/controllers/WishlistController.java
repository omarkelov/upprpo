package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponse;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponseConverter;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.entities.User;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.UserRepository;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class WishlistController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductResponseConverter productResponseConverter;

    @GetMapping(Endpoints.WISHLIST)
    public ResponseEntity<Set<ProductResponse>> getWishlist(@AuthenticationPrincipal UserDetails userDetails) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(user.getWishlist().stream()
            .map(productResponseConverter::mapToProductResponse)
            .collect(Collectors.toSet()));
    }

    @PutMapping(Endpoints.WISHLIST)
    public ResponseEntity<Set<ProductResponse>> addToWishlist(@AuthenticationPrincipal UserDetails userDetails, @RequestParam Long productId) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId).orElse(null);

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        user.getWishlist().add(product);

        userRepository.save(user);

        return ResponseEntity.ok(user.getWishlist().stream()
            .map(productResponseConverter::mapToProductResponse)
            .collect(Collectors.toSet()));
    }

    @DeleteMapping(Endpoints.WISHLIST)
    public ResponseEntity<Set<ProductResponse>> removeFromWishlist(@AuthenticationPrincipal UserDetails userDetails, @RequestParam Long productId) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElse(null);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Product product = productRepository.findById(productId).orElse(null);

        if (product == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        user.getWishlist().remove(product);

        userRepository.save(user);

        return ResponseEntity.ok(user.getWishlist().stream()
            .map(productResponseConverter::mapToProductResponse)
            .collect(Collectors.toSet()));
    }
}
