package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
	Optional<Category> findById(long id);
	List<Category> findAll();
	Optional<Category> findByName(String name);
	boolean existsByName(String name);
}
