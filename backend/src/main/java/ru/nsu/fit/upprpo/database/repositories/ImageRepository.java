package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.upprpo.database.entities.Image;
import ru.nsu.fit.upprpo.database.entities.Product;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findByProduct(Product product);

}
