package ru.nsu.fit.upprpo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.upprpo.config.JwtProvider;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.requests.AuthRequest;
import ru.nsu.fit.upprpo.controllers.requests.RegistrationRequest;
import ru.nsu.fit.upprpo.controllers.responses.AuthResponse;
import ru.nsu.fit.upprpo.database.entities.User;
import ru.nsu.fit.upprpo.security.services.UserService;

import javax.validation.Valid;

@RestController
public class AuthController {
    @Autowired
    private UserService userService;

    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping(Endpoints.REGISTER)
    public ResponseEntity<String> registerUser(@RequestBody @Valid RegistrationRequest registrationRequest) {
        if (userService.findByLogin(registrationRequest.getLogin()) != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        User user = new User();
        user.setPassword(registrationRequest.getPassword());
        user.setUsername(registrationRequest.getLogin());
        userService.saveUser(user);
        return ResponseEntity.ok("OK");
    }

    @PostMapping(Endpoints.LOGIN)
    public ResponseEntity<AuthResponse> auth(@RequestBody AuthRequest request) {
        User userEntity = userService.findByLoginAndPassword(request.getLogin(), request.getPassword());
        if (userEntity == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        String token = jwtProvider.generateToken(userEntity.getUsername());
        return ResponseEntity.ok(new ru.nsu.fit.upprpo.controllers.responses.AuthResponse(token));
    }
}
