package ru.nsu.fit.upprpo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.database.entities.RoleType;
import ru.nsu.fit.upprpo.database.repositories.UserRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtFilter jwtFilter;



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Let "http://localhost:8081/h2-console" work properly
        http.headers().frameOptions().disable();
        
        // Disable CSRF
        http = http
                .httpBasic().disable()
                .cors().and()
                .csrf().disable();

        // Set session management to stateless
        http = http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and();

        // Use antMatchers("/url/**").hasAuthority("ROLE_ADMIN") to authorize
        http.authorizeRequests()
                .antMatchers(Endpoints.CART).hasAuthority(RoleType.ROLE_CUSTOMER.toString())
                .antMatchers(Endpoints.WISHLIST).hasAuthority(RoleType.ROLE_CUSTOMER.toString())
                .antMatchers(Endpoints.CREATE_CATEGORY).hasAuthority(RoleType.ROLE_ADMIN.toString())
                .antMatchers(Endpoints.CREATE_PRODUCT).hasAuthority(RoleType.ROLE_ADMIN.toString())
                .antMatchers(Endpoints.RATING).hasAuthority(RoleType.ROLE_CUSTOMER.toString())
                .antMatchers(Endpoints.REVIEW).hasAuthority(RoleType.ROLE_CUSTOMER.toString())
                .antMatchers(Endpoints.UPLOAD_IMAGE).hasAuthority(RoleType.ROLE_ADMIN.toString())
                .and()
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
