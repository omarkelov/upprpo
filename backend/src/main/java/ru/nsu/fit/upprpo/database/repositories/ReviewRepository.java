package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.entities.Review;
import ru.nsu.fit.upprpo.database.entities.User;

import java.util.List;
import java.util.Optional;

public interface ReviewRepository extends CrudRepository<Review, Long> {

    Optional<Review> findByProductAndAuthor(Product product, User author);

    List<Review> findByProduct(Product product);

}
