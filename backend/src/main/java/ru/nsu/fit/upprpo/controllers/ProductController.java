package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.nsu.fit.upprpo.constants.Endpoints;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponse;
import ru.nsu.fit.upprpo.controllers.responses.ProductResponseConverter;
import ru.nsu.fit.upprpo.database.entities.Category;
import ru.nsu.fit.upprpo.database.entities.Product;
import ru.nsu.fit.upprpo.database.repositories.CategoryRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepository;
import ru.nsu.fit.upprpo.database.repositories.ProductRepositoryConnector;
import ru.nsu.fit.upprpo.database.repositories.RatingRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/", produces = "application/json")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private ProductResponseConverter productResponseConverter;

    final Object lock = new Object();

    private ProductRepositoryConnector productConnector;

    @Autowired
    private CategoryRepository categoryRepository;

    private synchronized ProductRepositoryConnector getRepositoryConnector() {
        if (productConnector == null) {
            productConnector = new ProductRepositoryConnector(productRepository);
        }
        return productConnector;
    }

    @GetMapping(Endpoints.PRODUCTS)
    public String getProducts() {
        List<ProductResponse> products = productRepository
                .findAll()
                .stream()
                .map(productResponseConverter::mapToProductResponse)
                .collect(Collectors.toList());
        return new Gson().toJson(products);
    }

    @GetMapping(Endpoints.PRODUCT + "/{productId}")
    public String getProduct(@PathVariable long productId) {
        ProductResponse product = productResponseConverter.mapToProductResponse(productRepository.findById(productId));
        return new Gson().toJson(product);
    }

    private List<Product> getProductsInCategoryRecursive(Category category) {
        var products = productRepository.findByCategory(category);
        var subcategories = category.getChildren();
        for (var sub : subcategories) {
            products.addAll(getProductsInCategoryRecursive(sub));
        }
        return products;
    }

    @GetMapping(Endpoints.PRODUCTS + "/{categoryId}")
    public String getProductsInCategory(@PathVariable long categoryId) {
        Category category = categoryRepository
                .findById(categoryId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        List<ProductResponse> products = getProductsInCategoryRecursive(category)
                .stream()
                .map(productResponseConverter::mapToProductResponse)
                .collect(Collectors.toList());
        return new Gson().toJson(products);
    }

    @GetMapping(Endpoints.PRODUCTS_SEARCH)
    public String searchProducts(
            @RequestParam(name = "productName") Optional<String> name,
            @RequestParam(name = "priceMin") Optional<BigDecimal> priceMin,
            @RequestParam(name = "priceMax") Optional<BigDecimal> priceMax,
            @RequestParam(name = "manufacturer") Optional<String> name1) {
        List<ProductResponse> products = getRepositoryConnector()
                .search(name, priceMin, priceMax, name1)
                .stream()
                .map(productResponseConverter::mapToProductResponse)
                .collect(Collectors.toList());
        return new Gson().toJson(products);
    }
}
