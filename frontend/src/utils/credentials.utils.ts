import { Credentials } from "../models/credentials";

export const getCredentials = () => {
    const login = localStorage.getItem('login');
    const token = localStorage.getItem('token');

    if (!login || !token) {
        return null;
    }

    return {login, token} as Credentials;
};

export const saveCredentials = (credentials: Credentials) => {
    localStorage.setItem('login', credentials.login);
    localStorage.setItem('token', credentials.token);
};

export const deleteCredentials = () => {
    localStorage.removeItem('login');
    localStorage.removeItem('token');
};
