import { FC, useCallback } from 'react';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { backendUrl } from '../../constants';
import { Product } from '../../models/product';
import { getCredentials } from '../../utils/credentials.utils';

import styles from './styles/buttons.module.scss';

const AddToWishlistButton: FC<{
    product: Product;
}> = ({ product }) => {
    const handleAddedToWishlist = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/wishlist?productId=${product.id}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        }).then(response => {
            if (response.ok) {
                NotificationManager.success(`The product "${product.name}" has been added to your wishlist`, response.status);
            } else {
                NotificationManager.error('Failed to add the product to your wishlist', response.status);
            }
        });
    }, [product.id, product.name]);

    return (
        <span onClick={handleAddedToWishlist} className={styles.actionButton}>Add to wishlist</span>
    );
};

export default AddToWishlistButton;
