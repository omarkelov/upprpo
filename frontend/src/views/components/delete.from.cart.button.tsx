import { FC, useCallback } from 'react';
// @ts-ignore
import { NotificationManager } from 'react-notifications';

import { backendUrl } from '../../constants';
import { Product } from '../../models/product';
import { getCredentials } from '../../utils/credentials.utils';

import styles from './styles/buttons.module.scss';

const DeleteFromCartButton: FC<{
    product: Product;
    onDeletedFromCart: (productId: number) => void;
}> = ({ product, onDeletedFromCart }) => {
    const handleDeletedFromWishlist = useCallback(() => {
        const credentials = getCredentials();

        if (!credentials) {
            NotificationManager.warning('You are not signed in', 'Error');
            return;
        }

        fetch(`${backendUrl}/cart?productId=${product.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${credentials.token}`
            }
        }).then(response => {
            if (response.ok) {
                onDeletedFromCart(product.id);
            } else {
                NotificationManager.error('Failed to delete the product from your cart', response.status);
            }
        });
    }, [onDeletedFromCart, product.id]);

    return (
        <div onClick={handleDeletedFromWishlist} className={styles.actionButton}>Delete</div>
    );
};

export default DeleteFromCartButton;
