import { FC } from 'react';
import { Product } from '../../../../models/product';
import ProductsItemView from '../../../components/products.item.view';
import CategoriesView from './categories.view';

import styles from '../styles/products.module.scss';


const ProductsView: FC<{
    products: Product[];
}> = ({ products }) => {
    return (
        <div className={styles.root}>
            <div className={styles.sidebar}>
                <CategoriesView />
                {/* <div className={styles.filters}>
                    TODO: filters
                </div> */}
            </div>
            <div className={styles.products}>
                {products.map((product, idx) =>
                    <ProductsItemView
                        key={idx}
                        product={product} />
                )}
            </div>
        </div>
    );
};

export default ProductsView;
