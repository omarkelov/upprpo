export interface Review {
    username: string;
    text: string;
    rating: number;
    timestamp: number;
}

export class ReviewModel implements Review {
    username = '';
    text = '';
    rating = 0;
    timestamp = 0;

    constructor(o: any) {
        this.username = o.username ?? this.username;
        this.text = o.text ?? this.text;
        this.rating = o.rating ?? this.rating;
        this.timestamp = o.timestamp ?? this.timestamp;
    }
}
