import { Product, ProductModel } from "./product";

export interface CartEntry {
    product: Product;
    quantity: number;
    price: number;
};

export class CartEntryModel implements CartEntry {
    product = new ProductModel({});
    quantity = 1;
    price = 0;

    constructor(o: any) {
        this.product = o.product ? new ProductModel(o.product) : this.product;
        this.quantity = o.quantity ?? this.quantity;
        this.price = o.price ?? this.price;
    }
}
